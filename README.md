# neovim-nightly-docker

## Why ?

- Keep track of all external tools, specifically installed for neovim
- Keep track of all installed plugins
- Easily deploy your developement environnement on different machine with
  docker
- Safely try new tools (e.g. language server protocol) in the sandbox environment

## Usage

`cd` into your top level project directory, then run the following docker
command:

```
docker run \
  --rm -it \
  [-e UID="1000" \]
  [-e GID="1000" \]
  [-v <your init.vim directory>:/home/neovim/.config/nvim \]
  -v <your workspace top level dir>:/mnt/workspace \
   registry.gitlab.com/catdevman/neovim-nightly-docker:latest \
  [nvim arguments]
```

### Examples

* Open neovim with file1 and use your custom neovim configuration stored in the
  `.dotfile` directory under your `$HOME`, if yours is different then use that path instead...
  You might also find it useful to set that path as an environment variable like NVIM_CONFIG_PATH=$HOME/.dotfiles/nvim
  and uses $NVIM_CONFIG_PATH instead:

```
docker run \
    --rm -it \
    -v $(pwd):/mnt/workspace \
    -v $HOME/.dotfiles/nvim:/home/neovim/.config/nvim \
    registry.gitlab.com/catdevman/neovim-nightly-docker:latest \
    file1
```

* File permission issues may arise if the default `user id (1000)` and `group
  id (1000)` of the container does not match user id and group id of the host.

```
docker run \
    --rm -it \
    -v $(pwd):/mnt/workspace \
    -e UID="1003" \
    -e GID="1004" \
    registry.gitlab.com/catdevman/neovim-nightly-docker:latest \
    -o file1 file2
```

You can find out your host user id and group id with the following command: `$ id`

## Local runtime/binary

For conveniance, you might want to define a function in your shell
configuration (bashrc, zshrc,…) to run neovim-docker as an executable, e.g.:

```
nvim() {
    docker run \
        --rm -it \
        -v $(pwd):/mnt/workspace \
        -v $HOME/.dotfiles/nvim:/home/neovim/.config/nvim \
        registry.gitlab.com/catdevman/neovim-nightly-docker:latest \
        "$@"
       }
```


## TODO
- [ ] Add good sane default for different image types. One for ts/node/js, python, and go (these are my preferred lanagues but others would be welcome as separate images/tags).  These images would have lsp setup for the language with some other nice defaults for that language. The intention with this would be to be very opinioned to make getting up and running easy but I always want a way to get back to being complete customized, that will be the base image with the lastest tag.
- [ ] Add a way to tag the base image with the version of nvim in it (like `v0.5.0-dev+1295-g3fc71ea22`). Also a separate tag with the date (like `2021.07.10` or `2021-07-10`) 

## Limitation

You must `cd` into the directory (preferably the top level directory of your
project) where the files you want to edit are located.

## Related project

* https://github.com/nicodebo/neovim-docker
