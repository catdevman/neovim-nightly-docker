FROM ubuntu:noble-20231214

ENV \
	UID="1000" \
	GID="1000" \
	UNAME="neovim" \
	GNAME="neovim" \
	SHELL="/bin/bash" \
	WORKSPACE="/mnt/workspace" \
	NVIM_CONFIG="/home/neovim/.config/nvim" \
	NVIM_PCK="/home/neovim/.local/share/nvim/site/pack" \
	ENV_DIR="/home/neovim/.local/share/vendorvenv" \
	NVIM_PROVIDER_PYLIB="python3_neovim_provider" \
	PATH="/home/neovim/.local/bin:/home/neovim/tools/nvim/bin:${PATH}" \
	DEBIAN_FRONTEND="noninteractive"

RUN \
	apt update -y \
	&& apt install -y \
	git \
	curl \
	sudo \
	python3 \
	fzf \
	build-essential \
	python3-dev \
	gcc \
	git \
	software-properties-common \
	dirmngr \
	apt-transport-https \
	lsb-release \
	ca-certificates \
	golang \
	python3-six

RUN add-apt-repository ppa:neovim-ppa/unstable

RUN apt update -y && apt install -y neovim
RUN curl -fLo $NVIM_CONFIG/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
RUN	addgroup "${GNAME}" \
	&& adduser --disabled-password --ingroup "${GNAME}"  --shell "${SHELL}" "${UNAME}" \
    && echo "${UNAME} ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
	&& chown -R neovim:neovim /home/neovim

RUN go install golang.org/x/tools/gopls@latest

COPY entrypoint.sh /usr/local/bin/

VOLUME "${WORKSPACE}"
VOLUME "${NVIM_CONFIG}"

ENTRYPOINT ["sh", "/usr/local/bin/entrypoint.sh"]
