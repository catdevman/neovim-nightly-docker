#!/usr/bin/env sh

usermod -u "${UID}" neovim
groupmod -g "${GID}" neovim
su -c "nvim +PlugInstall +qa" neovim
cd "${WORKSPACE}" && su -c "nvim $@" neovim
